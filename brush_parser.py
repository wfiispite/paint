import xml.dom.minidom
import log_builder


class BrushParser():
 
    def __init__(self):
        self.brush_data = ()

    @log_builder.get_function_name_info
    def brush_parser(self):

        try:
            DOMTree = xml.dom.minidom.parse('Brushes.xml')
            collection = DOMTree.documentElement

            brushes = collection.getElementsByTagName("brush")

            tuple_list = []

            for brush in brushes:

                if brush.hasAttribute("id"):
                    id = brush.getAttribute("id")

                size = brush.getElementsByTagName('size')[0]
                size_data = int(size.childNodes[0].data)

                self.brush_data = (size_data)
                yield self.brush_data

            for t in tuple_list:
                print(t)

        except IOError:
            log_builder.logging.error("Nie mozna otworzyc pliku")

if __name__ == '__main__':
    brush = BrushParser()
    brush.brush_parser()
    for x in brush.brush_parser():
        print x
        print type(x)
