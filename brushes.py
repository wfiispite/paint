size_small = 1
size_average = 3
size_big = 6
size_very_big = 10
size_large = 15

brush_table = [size_small, size_average, size_big, size_very_big, size_large]