import xml.dom.minidom
import log_builder


class ColourParser():

    def __init__(self):
        self.colour_data = ()

    @log_builder.get_function_name_info
    def colour_parser(self):

        try:
            DOMTree = xml.dom.minidom.parse('Colours.xml')
            collection = DOMTree.documentElement

            colours = collection.getElementsByTagName("colour")

            for colour in colours:

                if colour.hasAttribute("id"):
                    id = colour.getAttribute("id")

                red = colour.getElementsByTagName('red')[0]
                red_data = int(red.childNodes[0].data)

                green = colour.getElementsByTagName('green')[0]
                green_data = int(green.childNodes[0].data)

                blue = colour.getElementsByTagName('blue')[0]
                blue_data = int(blue.childNodes[0].data)

                self.colour_data = (red_data, green_data, blue_data)

                yield self.colour_data

        except IOError:
            log_builder.logging.error("Nie mozna otworzyc pliku")

if __name__ == '__main__':
    colour = ColourParser()
    colour.colour_parser()
    for x in colour.colour_parser():
        print x
        print type(x)



