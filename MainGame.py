import pygame
from pygame.locals import *
from Drawer import Drawer
from ParametersGetter import ParametersGetter
import log_builder
from Buttons import Button


class MainGame():

    def __init__(self, game_status, higth, width):

        self.game_status = game_status
        self.draw_on = False
        self.default_color = (0, 0, 0)
        self.default_brush_size = 1
        self.white = (255, 255, 255)
        self.higth = higth
        self.width = width

    @log_builder.get_function_name_info
    def start_game(self):

        screen = pygame.display.set_mode((self.width, self.higth))
        screen.fill((self.white))

        color = None
        radius = None

        object1 = Drawer(screen)
        while self.game_status:
            pygame.init()

            if color is None:
                color = self.default_color

            if radius is None:
                radius = self.default_brush_size

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()

                if event.type == pygame.MOUSEBUTTONDOWN:
                    Drawer.draw_circle(object1, color, event.pos, radius)
                    self.draw_on = True

                if event.type == pygame.MOUSEBUTTONUP:
                    self.draw_on = False
                if event.type == pygame.MOUSEMOTION:
                    if self.draw_on:

                        Drawer.draw_circle(object1, color, event.pos, radius)
                        Drawer.roundline(object1, color, event.pos, last_pos,  radius)

                    last_pos = event.pos
                    pygame.display.flip()

if __name__ == '__main__':

    game = MainGame(True, 1000, 500)
    game.start_game()
    