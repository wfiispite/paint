from unittest import TestCase
from Threading.CancelToken import CancelToken
from Threading.UnitTests.MockUnitOfWork import MockUnitOfWork
from Threading.ThreadUnitOfWork import ThreadUnitOfWork

class ThreadUnitOfWorkTest(TestCase):
    def test_canBeExecuted(self):
        # arrange
        unitOfWork = MockUnitOfWork()
        unitOfWorkThread = ThreadUnitOfWork(unitOfWork)

        # act
        unitOfWorkThread.start()
        unitOfWorkThread.stop()
        unitOfWorkThread.join(5)

        # assert
        self.assertTrue(unitOfWork.executed)

    def test_canBeStartedAndStopped(self):
        # arrange
        unitOfWork = MockUnitOfWork()
        unitOfWorkThread = ThreadUnitOfWork(unitOfWork)

        # act
        unitOfWorkThread.start()
        started = unitOfWorkThread.isAlive()

        unitOfWorkThread.stop()
        unitOfWorkThread.join(5)

        # assert
        self.assertTrue(started)
        self.assertFalse(unitOfWorkThread.isAlive())

