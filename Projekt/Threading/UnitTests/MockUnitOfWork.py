from Threading.UnitOfWork import  UnitOfWork

class MockUnitOfWork(UnitOfWork):
    def __init__(self):
        self.stop = False
        self.executed = False

    def execute(self):
        self.executed = True
        if(self.stop):
            self._cancel_token.cancel()

