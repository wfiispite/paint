from unittest import TestCase
from Threading.CancelToken import CancelToken

class CancelTokenTest(TestCase):
    def test_shouldBeCancelledOnInstantiation(self):
        #Arrange
        cancel_token = CancelToken()

        #Act

        #Assert
        self.assertFalse(cancel_token.is_cancelled())

    def test_shouldBeCancelledUponCancel(self):
        #Arrange
        cancel_token = CancelToken()

        #Act
        cancel_token.cancel()

        #Assert
        self.assertTrue(cancel_token.is_cancelled())