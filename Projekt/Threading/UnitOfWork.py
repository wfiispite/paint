from abc import abstractmethod


class UnitOfWork:
    def __init__(self):
        self._cancel_token = None

    @abstractmethod
    def execute(self):
        pass

    def set_cancel_token(self, token):
        self._cancel_token = token