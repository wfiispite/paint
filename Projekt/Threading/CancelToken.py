class CancelToken:
    def __init__(self, cancel_token=False):
        self._is_cancelled = cancel_token

    def is_cancelled(self):
        return self._is_cancelled

    def cancel(self):
        self._is_cancelled = True
