from threading import Thread

from Threading.CancelToken import CancelToken


class ThreadUnitOfWork(Thread):
    def __init__(self, unit_of_work):
        super(ThreadUnitOfWork, self).__init__()

        self._unit_of_work = unit_of_work
        self._cancelToken = CancelToken()
        self._unit_of_work.set_cancel_token(self._cancelToken)

    def run(self):
        while not self._cancelToken.is_cancelled():
            self._unit_of_work.execute()

    def stop(self):
        self._cancelToken.cancel()