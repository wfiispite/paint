import cv2
import collections
import numpy as np

class ObjectTracker:
    def __init__(self, configuration):
        self.configuration = configuration
        self.colorLower = configuration.trackingColorLowerHSV
        self.colorUpper = configuration.trackingColorUpperHSV
        self.pts = collections.deque()
        self.center = None

    def get_pointer_center(self):
        return self.center

    def process(self, frame):
        frame = cv2.resize(frame, (self.configuration.window_width, self.configuration.window_height))
        blurred = cv2.GaussianBlur(frame, (11, 11), 0)
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(hsv, self.colorLower, self.colorUpper)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)

        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)[-2]
        self.center = None

        if len(cnts) > 0:
            c = max(cnts, key=cv2.contourArea)
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            self.center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            if radius > 10:
                cv2.circle(frame, (int(x), int(y)), int(radius),
                    (0, 255, 255), 2)
                cv2.circle(frame, self.center, 5, (0, 0, 255), -1)
                self.pts.appendleft(self.center)
            else:
                self.center = (-1, -1)

        if self.center:
            cv2.putText(frame, "x: {}, y: {}".format(self.center[0], self.center[1]),
                (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
                0.35, (0, 0, 255), 1)

        cv2.imshow("Podglad", frame)
        key = cv2.waitKey(1) & 0xFF
