import pygame

from InputProcessor.FrameProcessor.ObjectTracker import ObjectTracker
from VideoCapturer import VideoCapturer
from ObjectTracker import ObjectTracker

from Gestures.GestureFactory import GestureFactory

class FrameProcessor:
    def __init__(self, drawable, configuration):
        self._drawable = drawable
        self.configuration = configuration
        self._gesture_factory = GestureFactory(drawable)
        self.vc = VideoCapturer(configuration)
        self._ObjectTracker = ObjectTracker(configuration)


    def process(self):
        frame = self.vc.capture_frame()
        self._ObjectTracker.process(frame)
        position = self._ObjectTracker.get_pointer_center()
        if position:
            if self.is_in_drawing_range(position):
                gesture = self._gesture_factory.get_gesture('draw_pixel')
                gesture.perform(position[0], position[1])
            if self.is_in_option_range(position):
                gesture = self._gesture_factory.get_gesture('pick_option')
                gesture.perform(position[0], position[1])

    def is_in_drawing_range(self, position):
        if 0 <= position[0] <= self.configuration.window_width - self.configuration.color_box_size:
            if 0 <= position[1] <= self.configuration.window_height:
                return True
        return False

    def is_in_option_range(self, position):
        if self.configuration.window_width - self.configuration.color_box_size <= position[0] <= self.configuration.window_width:
             if 0 <= position[1] <= self.configuration.window_height:
                return True
        return False
