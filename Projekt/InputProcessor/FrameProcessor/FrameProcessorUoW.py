from Threading.UnitOfWork import UnitOfWork

class FrameProcessorUoW(UnitOfWork):
    def __init__(self, frame_processor):
        self._frame_processor = frame_processor

    def execute(self):
        self._frame_processor.process()