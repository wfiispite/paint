from unittest import TestCase
from Window.Configuration import Configuration
from InputProcessor.FrameProcessor.VideoCapturer import VideoCapturer
from InputProcessor.FrameProcessor.ObjectTracker import ObjectTracker
from InputProcessor.FrameProcessor.FrameProcessor import FrameProcessor
from Gestures.GestureFactory import GestureFactory
from Gestures import DrawPixelGesture
from Gestures import PickOptionGesture

from mock import Mock
from InputProcessor.FrameProcessor.tests.FrameProcessorMock import FrameProcessorMock

__author__ = 'ian'


class TestFrameProcessor(TestCase):
    def __init__(self, *args, **kwargs):
        super(TestFrameProcessor, self).__init__(*args, **kwargs)

        self.mockconf = Mock(spec=Configuration)
        self.mockconf.window_width = 500
        self.mockconf.window_height = 500
        self.mockconf.color_box_size = 50
        self.mockconf.default_brush_size = 100
        self.expectedgesturecall = 'draw_pixel'

        self.mockvc = Mock(spec=VideoCapturer)
        self.mockvc.capture_frame.return_value = None

        self.mockot = Mock(spec=ObjectTracker)
        self.mockot.get_pointer_center.return_value = (100, 100)
        self.mockot.process.return_value = None


        self.mockgf = Mock(spec=GestureFactory)

    def test_is_in_option_range(self):
        proc = FrameProcessor(None, self.mockconf)
        self.assertTrue(proc.is_in_option_range((450, 500)))
        self.assertTrue(proc.is_in_option_range((500, 500)))
        self.assertFalse(proc.is_in_option_range((-1, -1)))
        self.assertFalse(proc.is_in_option_range((2000, 2000)))
        self.assertFalse(proc.is_in_option_range((100, 100)))

    def test_is_in_drawing_range(self):
        proc = FrameProcessor(None, self.mockconf)
        self.assertTrue(proc.is_in_drawing_range((450, 500)))
        self.assertTrue(proc.is_in_drawing_range((0, 0)))
        self.assertFalse(proc.is_in_drawing_range((-1, -1)))
        self.assertFalse(proc.is_in_drawing_range((2000, 2000)))
        self.assertFalse(proc.is_in_drawing_range((475, 475)))

    def test_process_should_perform_right_gesture(self):
        frameprocessormock = FrameProcessorMock(self.mockot, self.mockgf, self.mockvc, self.mockconf)
        frameprocessormock.process()
        self.mockgf.get_gesture.assert_called_with(self.expectedgesturecall)
