from InputProcessor.FrameProcessor.FrameProcessor import FrameProcessor

__author__ = 'ian'


class FrameProcessorMock(FrameProcessor):
    def __init__(self, objecttrackermock, gesturefactorymock, videocapturemock, configurationmock):
        self._drawable = None
        self.configuration = configurationmock
        self._gesture_factory = gesturefactorymock
        self.vc = videocapturemock
        self._ObjectTracker = objecttrackermock
