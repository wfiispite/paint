import numpy as np
import cv2

class VideoCapturer():
    def __init__(self, configuration):
        self.cap = cv2.VideoCapture(0)
        self.frame = np.ndarray([])

        self.xsize = configuration.window_width
        self.ysize = configuration.window_height

        self.cap.set(3, self.xsize)
        self.cap.set(4, self.ysize)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.cap.release()
        cv2.destroyAllWindows()

    def capture_frame(self):
        var, self.frame = self.cap.read()
        try:
            assert type(self.frame) == np.ndarray
        except AssertionError:
            print("No camera detected")
        else:
            return self.frame