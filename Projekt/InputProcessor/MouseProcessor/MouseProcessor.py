import pygame
from Gestures.GestureFactory import GestureFactory

class MouseProcessor:
    def __init__(self, drawable):
        self._drawable = drawable
        self._gesture_factory = GestureFactory(drawable)

        self._right_button_pressed = False

    def process(self):
        if pygame.mouse.get_pressed()[0]:
            position = pygame.mouse.get_pos()
            gesture = self._gesture_factory.get_gesture('draw_pixel')
            gesture.perform(position[0], position[1])
        if pygame.mouse.get_pressed()[2]:
            self._right_button_pressed = True
        if self._right_button_pressed and not pygame.mouse.get_pressed()[2]:
            position = pygame.mouse.get_pos()
            gesture = self._gesture_factory.get_gesture('pick_option')
            gesture.perform(position[0], position[1])
            self._right_button_pressed = False