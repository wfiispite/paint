from unittest import TestCase
from mock import Mock
from mock import patch
import pygame
from Gestures.GestureFactory import GestureFactory
from InputProcessor.MouseProcessor.tests.MouseProcessorMock import MouseProcessorMock

__author__ = 'ian'


class TestMouseProcessor(TestCase):
    def __init__(self, *args, **kwargs):
        super(TestMouseProcessor, self).__init__(*args, **kwargs)
        self.mockgf = Mock(spec=GestureFactory)

    @patch('pygame.mouse.get_pressed', return_value=(True, False, False))
    @patch('pygame.mouse.get_pos', return_value=(100, 100))
    def test_process_should_perform_right_gesture(self, mock, mock2):
        mouseprocessormock = MouseProcessorMock(self.mockgf)
        mouseprocessormock.process()
        self.mockgf.get_gesture.assert_called_with('draw_pixel')

    @patch('pygame.mouse.get_pressed', return_value=(False, False, False))
    @patch('pygame.mouse.get_pos', return_value=(630, 470))
    def test_process_should_perform_right_gesture(self, mock, mock2):
        mouseprocessormock = MouseProcessorMock(self.mockgf)
        mouseprocessormock._right_button_pressed = True
        mouseprocessormock.process()
        self.mockgf.get_gesture.assert_called_with('pick_option')

    @patch('pygame.mouse.get_pressed', return_value=(True, False, False))
    @patch('pygame.mouse.get_pos', return_value=(700, 700))
    def test_process_should_perform_right_gesture(self, mock, mock2):
        mouseprocessormock = MouseProcessorMock(self.mockgf)
        mouseprocessormock._right_button_pressed = False
        mouseprocessormock.process()
        assert not self.mockgf.get_gesture.called
