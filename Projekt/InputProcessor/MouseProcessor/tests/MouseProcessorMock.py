from InputProcessor.MouseProcessor.MouseProcessor import MouseProcessor


__author__ = 'ian'


class MouseProcessorMock(MouseProcessor):
    def __init__(self, gesturefactorymock):
        self._drawable = None
        self._gesture_factory = gesturefactorymock
        self._right_button_pressed = False