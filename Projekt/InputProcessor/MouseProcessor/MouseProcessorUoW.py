from Threading.UnitOfWork import UnitOfWork

class MouseProcessorUoW(UnitOfWork):
    def __init__(self, mouse_processor):
        self._mouse_processor = mouse_processor

    def execute(self):
        self._mouse_processor.process()