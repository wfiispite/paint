from InputProcessor.FrameProcessor.FrameProcessor import FrameProcessor
from InputProcessor.FrameProcessor.FrameProcessorUoW import FrameProcessorUoW
from InputProcessor.MouseProcessor.MouseProcessor import MouseProcessor
from InputProcessor.MouseProcessor.MouseProcessorUoW import MouseProcessorUoW
from Threading.ThreadUnitOfWork import ThreadUnitOfWork


class InputProcessorFactory:
    def __init__(self, drawable, configuration):
        self.drawable = drawable
        self.configuration = configuration

    def create(self, type):
        if type == "mouse":
            mouse_processor = MouseProcessor(self.drawable)
            mouse_processor_uow = MouseProcessorUoW(mouse_processor)
            mouse_processor_thread = ThreadUnitOfWork(mouse_processor_uow)
            return mouse_processor_thread
        if type == "camera":
            frame_processor = FrameProcessor(self.drawable, self.configuration)
            frame_processor_uow = FrameProcessorUoW(frame_processor)
            frame_processor_thread = ThreadUnitOfWork(frame_processor_uow)
            return frame_processor_thread