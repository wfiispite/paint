import sys, pygame, os
from Window.IDrawable import IDrawable

class MainWindow(IDrawable):
    def __init__(self, configuration):
        self._background_color = configuration.background_color
        self._size = (configuration.window_width, configuration.window_height)
        self._screen = pygame.display.set_mode(self._size)
        self._screen.fill(configuration.background_color)
        self._current_color = configuration.default_color
        self._brush_size = configuration.default_brush_size
        self._available_colors = configuration.available_colors

        #UI
        self._color_box_size = configuration.color_box_size
        basePath = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..', 'Resources'))
        up_size_path = os.path.join(basePath, "up_size.png")
        down_size_path = os.path.join(basePath, "down_size.png")
        self._up_size_box_img = pygame.image.load(up_size_path)
        self._down_size_box_img = pygame.image.load(down_size_path)

    def _draw_color_picker(self):
        x = self._size[0] - self._color_box_size
        y = 0
        for color in self._available_colors:
            self.draw_rectangle(x, y, color, self._color_box_size, self._color_box_size)
            y = y + self._color_box_size

    def _draw_size_picker(self):
        x = self._size[0] - self._color_box_size
        y = len(self._available_colors) * self._color_box_size
        self._screen.blit(self._up_size_box_img, (x, y))
        y = y + self._color_box_size
        self._screen.blit(self._down_size_box_img, (x, y))

    def _evaluate_what_option_has_been_chosen(self, x, y):
        if x >= self._size[0] - self._color_box_size and x < self._size[0]:
            if y >= 0 and y <= len(self._available_colors) * self._color_box_size:
                color_chosen = y/self._color_box_size
                self._current_color = self._available_colors[color_chosen]
            if y >= len(self._available_colors) * self._color_box_size and y <= self._size[1]:
                y = self._size[1] - y
                option_chosen = y / self._color_box_size
                if option_chosen == 1:
                    self._brush_size = self._brush_size + 1
                if option_chosen == 0:
                    if(self._brush_size > 2):
                        self._brush_size = self._brush_size - 1

    def run(self):
        self._draw_color_picker()
        self._draw_size_picker()
        pygame.display.flip()

    def draw_pixel(self, x, y):
        pygame.draw.rect(self._screen, self._current_color, pygame.Rect(x-self._brush_size/2, y-self._brush_size/2, self._brush_size, self._brush_size))

    def draw_rectangle(self, x, y, color, width, height):
        pygame.draw.rect(self._screen, color, pygame.Rect(x, y, width, height))

    def pick_option(self, x, y):
        self._evaluate_what_option_has_been_chosen(x, y)
