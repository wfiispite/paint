from abc import abstractmethod

class IDrawable:
    @abstractmethod
    def draw_pixel(self, x, y):
        pass