from pygame import Color

class Configuration:
    window_width = 640
    window_height = 480
    background_color = Color(255,255,255)
    default_color = Color(0, 0, 0)
    trackingColorLowerHSV = (29, 86, 6) #GREEN FOR DEFAULT
    trackingColorUpperHSV = (64, 255, 255) #GREEN FOR DEFAULT
    color_box_size = 60
    default_brush_size = 5
    available_colors = [
        Color(0, 0, 0),
        Color(255, 255, 255),
        Color(255, 0, 0),
        Color(0, 255, 0),
        Color(0, 0, 255),
        Color(255, 0, 0)
    ]
