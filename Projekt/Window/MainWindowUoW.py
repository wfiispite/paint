from Threading.UnitOfWork import UnitOfWork

class MainWindowUoW(UnitOfWork):
    def __init__(self, main_window):
        self._main_window = main_window

    def execute(self):
        self._main_window.run()