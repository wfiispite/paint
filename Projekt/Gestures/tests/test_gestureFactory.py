from unittest import TestCase
from Gestures.GestureFactory import GestureFactory
from Gestures.DrawPixelGesture import DrawPixelGesture
from Gestures.PickOptionGesture import PickOptionGesture

__author__ = 'ian'


class TestGestureFactory(TestCase):
    def test_get_gesture_should_return_proper_gesture(self):
        gesture = "draw_pixel"
        drawable = None
        gesture_factory = GestureFactory(drawable)
        self.assertTrue(isinstance(gesture_factory.get_gesture(gesture), DrawPixelGesture))

        gesture = "pick_option"
        self.assertTrue(isinstance(gesture_factory.get_gesture(gesture), PickOptionGesture))