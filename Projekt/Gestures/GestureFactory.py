from Gestures.DrawPixelGesture import DrawPixelGesture
from Gestures.PickOptionGesture import PickOptionGesture


class GestureFactory:
    def __init__(self, drawable):
        self._drawable = drawable

    def get_gesture(self, gesture_name):
        if(gesture_name == 'draw_pixel'):
            return DrawPixelGesture(self._drawable)
        if(gesture_name == 'pick_option'):
            return PickOptionGesture(self._drawable)