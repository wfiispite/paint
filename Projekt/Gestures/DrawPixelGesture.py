from IGesture import IGesture

class DrawPixelGesture(IGesture):
    def __init__(self, drawable):
        self._drawable = drawable

    def perform(self, x, y):
        self._drawable.draw_pixel(x, y)