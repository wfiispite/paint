from IGesture import IGesture

class PickOptionGesture(IGesture):
    def __init__(self, drawable):
        self._drawable = drawable

    def perform(self, x, y):
        self._drawable.pick_option(x, y)