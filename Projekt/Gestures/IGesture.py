from abc import abstractmethod

class IGesture:
    @abstractmethod
    def perform(self):
        pass