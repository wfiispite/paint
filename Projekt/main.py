from InputProcessor.InputProcessorFactory import InputProcessorFactory
from Window.Configuration import Configuration
from Window.MainWindow import MainWindow
from Threading.ThreadUnitOfWork import ThreadUnitOfWork
from Window.MainWindowUoW import MainWindowUoW
import pygame

if __name__ == "__main__":
    processor = "mouse" # TYPE IN "mouse" for MouseProcessor

    pygame.init()
    configuration = Configuration()

    main_window = MainWindow(configuration)
    main_window_uow = MainWindowUoW(main_window)
    main_window_thread = ThreadUnitOfWork(main_window_uow)

    processor_factory = InputProcessorFactory(main_window, configuration)
    thread =  processor_factory.create(processor)

    main_window_thread.start()
    thread.start()

    done = False
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                thread.stop()
                while thread.isAlive():
                    pass
                main_window_thread.stop()
                while main_window_thread.isAlive():
                    pass
                done = True

    pygame.quit()

