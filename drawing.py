from colours import color_table
from brushes import brush_table
import pygame
from DrawExceptions import ColorTableIndexException, BrushTableIndexException



screen = pygame.display.set_mode((1000,1000))
screen.fill((255,255,255))



class DrawCircle():


    def __init__(self):

        self.color = (0, 0, 0)
        self.radius = 0
        self.background = (255, 255, 255)




    def choose_parameters(self,color_number, brush_number, background_color):

        if color_number in range(25):
            self.color = color_table[color_number -1]
        else:
            raise ColorTableIndexException
        if brush_number in range(6):
            self.radius = brush_table[brush_number - 1]
        else:
            raise BrushTableIndexException
        if background_color in range(25):
            self.background = color_table[color_number -1]
        else:
            raise ColorTableIndexException



    def draw(self, position):

        #screen.fill(self.background)

        while 1:
            brush_menu = pygame.image.load('brush.png')
            screen.blit(brush_menu, (800, -70))

            color_menu = pygame.image.load('color.png')
            screen.blit(color_menu, (900, 180))
            pygame.display.flip()


            pygame.init()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()

            pygame.draw.circle(screen, self.color, position, self.radius)


            pygame.display.flip()

object  = DrawCircle()

try:

    object.choose_parameters(13,5, 4)
except ColorTableIndexException:
    print 'Color nuber in range 1-25'
except BrushTableIndexException:
    print 'Brush nuber in range 1-5'
object.draw((200,400))