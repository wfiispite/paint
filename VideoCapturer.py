import numpy as np
import cv2
import logging
import time


class VideoCapturer():
    ''' recommended usage through: >> with VideoCapturer() as var_name : <<    '''

    def __init__(self, xsize=640, ysize=480, size_param_nrs=[3,4]):
        self.cap = cv2.VideoCapture(-1)
        self.frame = np.ndarray([])
        logger.debug('object of class VideoCapturer created')

        # frame size setting, size_param_nrs may depend on device
        self.cap.set(size_param_nrs[0], xsize)
        self.cap.set(size_param_nrs[1], ysize)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logger.info('camera releasing...')
        self.cap.release()
        cv2.destroyAllWindows()

    def capture_frame(self):
        var, self.frame = self.cap.read()
        try:
            assert type(self.frame) == np.ndarray
        except AssertionError:
            logger.error('invalid frame capture from camera, returning None')
        else:
            return self.frame





# logging configuration

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

if __name__ != '__main__':
    ch.setLevel(logging.INFO)