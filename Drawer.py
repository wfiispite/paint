import pygame
import log_builder


class Drawer():

    @log_builder.get_function_name_info
    def __init__(self, screen):
        self.screen = screen

    @log_builder.get_function_name_info
    def draw_circle(self, line_color, position, radius):
        pygame.draw.circle(self.screen, line_color, position, radius)

    @log_builder.get_function_name_info
    def draw_line(self, point_one, point_two, width):
        pygame.draw.line(self.screen, self.color, point_one, point_two, width)

    @log_builder.get_function_name_info
    def draw_polygon(self, polygon_color, pointlist):
        pygame.draw.polygon(self.screen, polygon_color, pointlist)

    @log_builder.get_function_name_info
    def draw_rectangle(self, rectangle_color, left, top, width, height):
        pygame.draw.rect(self.screen, rectangle_color, pygame.Rect(left, top, width, height))

    @log_builder.get_function_name_info
    def roundline(self, color, start, end, radius):
        dx = end[0]-start[0]
        dy = end[1]-start[1]
        distance = max(abs(dx), abs(dy))
        for i in range(distance):
            x = int(start[0]+float(i)/distance*dx)
            y = int(start[1]+float(i)/distance*dy)
            pygame.draw.circle(self.screen, color, (x, y), radius)

    