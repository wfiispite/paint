import numpy as np
import cv2
from sys import argv
import logging
import time
from VideoCapturer import *



class ObjectTracker():

    def __init__(self, video_capturer=None):
        if isinstance(video_capturer, VideoCapturer):
            self.capturer = video_capturer
            logger.debug('object of class \'ObjectTracker\' created')
        else:
            logger.error('TypeError - invalid parameter passed as VideoCapturer object')
            raise TypeError

        self.lower_limit = (0,0,220)
        self.upper_limit = (179,15,250)
        self.active_roi = ([560,400], [640,480]) # x1,y1 x2,y2
        self.pointer_found = False
        self.pointer_position = ()
        self.pointer_in_roi = False

    def manual_hsv_calibration(self):
        logger.debug('manual hsv calibrater opened')

        def do_nothing(x):
            pass

        cv2.namedWindow('after cuts')

        # create trackbars for color change
        cv2.createTrackbar('Hl','after cuts',self.lower_limit[0],179,do_nothing)
        cv2.createTrackbar('Sl','after cuts',self.lower_limit[1],255,do_nothing)
        cv2.createTrackbar('Vl','after cuts',self.lower_limit[2],255,do_nothing)
        cv2.createTrackbar('Hu','after cuts',self.upper_limit[0],179,do_nothing)
        cv2.createTrackbar('Su','after cuts',self.upper_limit[1],255,do_nothing)
        cv2.createTrackbar('Vu','after cuts',self.upper_limit[2],255,do_nothing)

        prev = self.capturer.capture_frame()
        prev = cv2.cvtColor(prev,cv2.COLOR_BGR2GRAY)
        while True:
            hl = cv2.getTrackbarPos('Hl','after cuts')
            sl = cv2.getTrackbarPos('Sl','after cuts')
            vl = cv2.getTrackbarPos('Vl','after cuts')
            hu = cv2.getTrackbarPos('Hu','after cuts')
            su = cv2.getTrackbarPos('Su','after cuts')
            vu = cv2.getTrackbarPos('Vu','after cuts')

            lower_limit = np.array([hl,sl,vl])
            upper_limit = np.array([hu,su,vu])

            frame = self.capturer.capture_frame()
            frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
            # Thresholding
            mask = cv2.inRange(frame_hsv, lower_limit, upper_limit)
            res = cv2.bitwise_and(frame_hsv, frame_hsv, mask= mask)

            difference, prev = self.bg_substractaction(res, prev)

            cv2.imshow('original', frame)
            cv2.imshow('after cuts', res)
            cv2.imshow('difference', difference)

            key = cv2.waitKey(100) & 0xFF
            spacebar_nr = 32
            if key == spacebar_nr:
                break

        logger.debug('limits set to:' + str(lower_limit) + ' and ' + str(upper_limit))
        self.lower_limit = lower_limit
        self.upper_limit = upper_limit
        cv2.destroyAllWindows()
        return (lower_limit,upper_limit)

    def bg_substractaction(self, current_frame_hsv, prev_frame_bw):
        '''odejmuje dwie klatki podane jako argumenty
        i zwraca roznice oraz ostatnia klatke w black&white'''

        frame_hsv = current_frame_hsv
        prev = prev_frame_bw
        # Thresholding
        mask = cv2.inRange(frame_hsv, ot.lower_limit, ot.upper_limit)
        res = cv2.bitwise_and(frame_hsv, frame_hsv, mask= mask)

        res_bw = cv2.cvtColor(res,cv2.COLOR_HSV2BGR)
        res_bw = cv2.cvtColor(res,cv2.COLOR_BGR2GRAY)
        ret,res_bw  = cv2.threshold(res_bw,10,255,cv2.THRESH_BINARY)
        mask2 = cv2.bitwise_and(res_bw,prev)
        mask2 = cv2.bitwise_not(mask2)

        difference = cv2.bitwise_and(res,res,mask=mask2)
        #cv2.imshow('difference_bg_sub', difference)

        return (difference,res_bw)

    def noise_cut(self, frame_hsv):
        frame_bw = cv2.inRange(frame_hsv, self.lower_limit, self.upper_limit)

        size = 3
        kernel = np.ones((size,size),np.uint8)
        erosion = cv2.erode(frame_bw,kernel,iterations = 3)
        result = cv2.dilate(erosion,kernel,iterations = 1)
        #cv2.imshow('noise cut',frame_hsv)
        return result

    def find_pointer(self, pure_pointer_frame):
        '''  zwraca wspolrzedne wskaznika (x,y) lub None jesli nie znalazl '''
        #frame = self.capturer.capture_frame() # tylko do wyswietlania

        contours, hier = cv2.findContours(pure_pointer_frame,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

        cnt_areas = []
        bgr = [0,210,0]
        for con in contours:
            frame2 = cv2.drawContours(frame, [con], 0, bgr, 3)
            if bgr[1] > 30:
                bgr[1]-=30

            cnt_areas.append(cv2.contourArea(con))

        #if len(contours) > 1:
        #    logger.debug('found '+str(len(contours))+'(>1) contours')

        if len(contours) >= 1:
            biggest_cnt_index = cnt_areas.index(max(cnt_areas))
            cnt = contours[biggest_cnt_index]

            leftmost = tuple(cnt[cnt[:,:,0].argmin()][0])
            rightmost = tuple(cnt[cnt[:,:,0].argmax()][0])
            topmost = tuple(cnt[cnt[:,:,1].argmin()][0])
            bottommost = tuple(cnt[cnt[:,:,1].argmax()][0])

            middle = (0.5*leftmost[0] + 0.5*rightmost[0], 0.5*topmost[1] + 0.5*bottommost[1])
            middle = (int(middle[0]), int(middle[1]))

        else:
            middle = None

        return middle

    def process(self, current_frame, previous_frame):
        prev = cv2.cvtColor(previous_frame,cv2.COLOR_BGR2GRAY)
        frame_hsv = cv2.cvtColor(current_frame, cv2.COLOR_BGR2HSV)

        diff, prev = self.bg_substractaction(frame_hsv, prev)
        frame_no_noise = self.noise_cut(diff)
        middle = self.find_pointer(frame_no_noise)


        p1, p2 = tuple(self.active_roi[0]), tuple(self.active_roi[1])
        if middle:
            self.pointer_found = True
            self.pointer_position = middle
            if middle[0] > p1[0] and middle < p2[0] and middle[1] > p1[1] and middle[1] < p2[1]:
                self.pointer_in_roi = True
            else:
                self.pointer_in_roi = False

            cv2.circle(frame,(middle[0],middle[1]),4,(255,0,0),3)
            cv2.circle(frame,(middle[0],middle[1]),8,(0,255,255),3)
            cv2.circle(frame,(middle[0],middle[1]),12,(255,0,0),3)
        else:
            self.pointer_found = False
            # pointer_in_roi jest True jesli pointer zniknie w roi i nie pojawi sie gdzie indziej

        cv2.rectangle(frame,p1,p2,(0,0,255),5)
        cv2.imshow("get_pos", current_frame)


    def get_pointer_found(self):
        return self.pointer_found

    def get_pointer_in_roi(self):
        return self.pointer_in_roi

    def get_position(self):
        return self.pointer_position







# logging configuration
    
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

if __name__ != '__main__':
    ch.setLevel(logging.INFO)



# example of usage

if __name__ == '__main__':
    '''#
    handler = logging.FileHandler('ObjectTracker.log')
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    '''

    with VideoCapturer() as vc:
        ot = ObjectTracker(vc)
        ot.manual_hsv_calibration()
        prev = ot.capturer.capture_frame()

        while 1:
            frame = ot.capturer.capture_frame()
            ot.process(frame, prev)
            prev = frame

            k = cv2.waitKey(20) & 0xFF
            spacebar_nr = 32
            if k == ord('m'):
                # 'm' enables new calibration
                ot.manual_hsv_calibration()
                continue
            elif k == ord('p'):
                if_print = not if_print
            elif k == spacebar_nr:
                break







# TODO ? :
# 1) mozliwosc manualnego wycinania szumow, ustawiania param przez TrackBary
# 2) logiczne AND na kilku kolejnych klatkach w celu usuniecia migoczacego, gestego tla
# 3) uciaglenie ruchu, usrednienie, albo ustawienie limitu na odl miedzy kolejnymi polozeniami
# 4) aproksymacja konturu (Douglas-Pecker)
# 5) sumowanie kilku polozonych blisko konturow
# 6) mozliwosc wyciecia ROI z poszukiwan wskaznika - przypadek lsniacych na bialo powierzchni i zrodel swiatla

# TODO !:
# roznica dwoch kolejnych klatek
# automatyczna kalibracja
