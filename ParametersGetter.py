from colour_parser import ColourParser
from brush_parser import BrushParser
import log_builder


class ParametersGetter():

    def __init__(self):
        self.colours = None
        self.brushes = None
        
    @log_builder.get_function_name_info
    def get_colour(self, colour_number):
        colour = ColourParser()
        self.colours = enumerate(colour.colour_parser())
        for colour_index, color_RGB in self.colours:
            if colour_index == colour_number:
                return color_RGB
                
    @log_builder.get_function_name_info
    def get_brush_size(self, brush_number):
        size = BrushParser()
        self.brushes = enumerate(size.brush_parser())
        for brush_index, brush_size in self.brushes:
            if brush_index == brush_number:
                return (brush_size)

if __name__ == '__main__':
    colour_parameter = ParametersGetter()
    print colour_parameter.get_colour(5)

    brush_parameter = ParametersGetter()
    print brush_parameter.get_brush_size(2)